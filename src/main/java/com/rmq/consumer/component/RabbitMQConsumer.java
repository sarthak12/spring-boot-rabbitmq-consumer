package com.rmq.consumer.component;

import com.rmq.consumer.exception.InvalidSalaryException;
import com.rmq.consumer.model.Employee;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class RabbitMQConsumer {

    @RabbitListener(queues = "javaInUse.queue")
    public void recievedMessage(Employee employee) throws InvalidSalaryException {
        log.info("Recieved Message From RabbitMQ: " + employee);
        if (employee.getSalary() < 0) {
            throw new InvalidSalaryException();
        }
    }
}
